package com.sillyhat.learningjdk.thread.LockAndSynchronized;

import java.util.concurrent.CyclicBarrier;

/**
 * AtomicTest
 *
 * @author 徐士宽
 * @date 2017/3/8 14:23
 */
public class AtomicTest extends TestTemplate{

    public AtomicTest(String _id,int _round,int _threadNum,CyclicBarrier _cb){
        super( _id, _round, _threadNum, _cb);
    }

    /**
     * synchronized关键字不在方法签名里面，所以不涉及重载问题
     */
    @Override
    long getValue() {
        return super.countValueAtmoic.get();
    }

    @Override
    void sumValue() {
        super.countValueAtmoic.addAndGet(super.preInit[indexAtomic.get()%round]);
    }
}
