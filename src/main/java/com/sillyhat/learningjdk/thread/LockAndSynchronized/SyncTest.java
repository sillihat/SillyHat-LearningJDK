package com.sillyhat.learningjdk.thread.LockAndSynchronized;

import java.util.concurrent.CyclicBarrier;

/**
 * SyncTest
 *
 * @author 徐士宽
 * @date 2017/3/8 14:25
 */
public class SyncTest extends TestTemplate{
    public SyncTest(String _id,int _round,int _threadNum,CyclicBarrier _cb){
        super( _id, _round, _threadNum, _cb);
    }

    /**
     * synchronized关键字不在方法签名里面，所以不涉及重载问题
     */
    @Override
    synchronized long getValue() {
        return super.countValue;
    }
    @Override
    synchronized void sumValue() {
        super.countValue+=preInit[index++%round];
    }
}
