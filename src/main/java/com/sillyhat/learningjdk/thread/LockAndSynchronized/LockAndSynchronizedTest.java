package com.sillyhat.learningjdk.thread.LockAndSynchronized;

import java.util.concurrent.CyclicBarrier;

/**
 * LockAndSynchronizedTest
 *
 * @author 徐士宽
 * @date 2017/3/8 14:23
 */
public class LockAndSynchronizedTest {

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            int round = 100000 * (i + 1);
            int threadNum = 5 * (i + 1);
            CyclicBarrier cb = new CyclicBarrier(threadNum * 2 + 1);
            System.out.println("==========================");
            System.out.println("round:" + round + " thread:" + threadNum);
            test(round, threadNum, cb);
        }
        System.out.println("=========finish===============");
    }

    public static void test(int round,int threadNum,CyclicBarrier cyclicBarrier){
        System.out.println("循环次数：" + round + ";线程数量：" + threadNum);
        new SyncTest("Sync",round,threadNum,cyclicBarrier).testTime();
        new LockTest("Lock",round,threadNum,cyclicBarrier).testTime();
        new AtomicTest("Atom",round,threadNum,cyclicBarrier).testTime();
    }
}