package com.sillyhat.learningjdk;

/**
 * Test
 *
 * @author 徐士宽
 * @date 2017/3/9 11:42
 */
public class Test {

    public static void main(String[] args) {
        try {
            Thread.sleep(5000);
            System.out.println("Hello,Java,谁是这个世界上最帅的人？");
            Thread.sleep(1000);
            System.out.println("查询中...");
            Thread.sleep(1000);
            System.out.print("正在调用google接口");
            for (int i = 0; i < 5; i++) {
                Thread.sleep(300);
                System.out.print(".");
            }
            System.out.println("");
            System.out.print("正在调用百度接口");
            for (int i = 0; i < 5; i++) {
                Thread.sleep(300);
                System.out.print(".");
            }
            System.out.println("");
            Thread.sleep(1000);
            System.out.println("查询结束：徐士宽是这个世界上最帅的人。");
            Thread.sleep(1000);
            System.out.println("王旭说徐士宽不帅");
            Thread.sleep(1000);
            System.out.println("请让王旭同学，速去医院检查眼睛。");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
