package com.sillyhat.learningjdk.overrideandoverload;

/**
 * Overload 重载
 *
 * @author 徐士宽
 * @date 2017/3/8 14:10
 */
public class Overload extends Parent{

    /**
     1、在使用重载时只能通过不同的参数样式。例如，不同的参数类型，不同的参数个数，不同的参数顺序（当然，同一方法内的几个参数类型必须不一样，例如可以是fun(int, float)， 但是不能为fun(int, int)）；
     2、不能通过访问权限、返回类型、抛出的异常进行重载；
     3、方法的异常类型和数目不会对重载造成影响；
     4、对于继承来说，如果某一方法在父类中是访问权限是priavte，那么就不能在子类对其进行重载，如果定义的话，也只是定义了一个新方法，而不会达到重载的效果。
     */
    public String parentOneTest(String src,int i){
        return src + i;
    }
}
