package com.sillyhat.learningjdk.overrideandoverload.question;

/**
 * ParentQuestion
 *
 * @author 徐士宽
 * @date 2017/3/8 14:12
 */
public class ParentQuestion {

    private String privateParentSrc = "ParentPrivateString";

    public String publicParentSrc = "ParentPublicString";

}
