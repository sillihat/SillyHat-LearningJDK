package com.sillyhat.learningjdk.overrideandoverload.question;

/**
 * Question1
 *
 * @author 徐士宽
 * @date 2017/3/8 14:12
 */
public class Question1 extends ParentQuestion{

    public String privateParentSrc = "subString";

    public String publicParentSrc = "subPublicString";

    public static void main(String[] args) {
        System.out.println(new Question1().privateParentSrc);
        System.out.println(new Question1().publicParentSrc);
    }
}
