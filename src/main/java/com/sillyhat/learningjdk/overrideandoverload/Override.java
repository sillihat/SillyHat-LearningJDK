package com.sillyhat.learningjdk.overrideandoverload;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Override 重写
 1、方法名、参数、返回值相同。
 ​2、子类方法不能缩小父类方法的访问权限。
 3、子类方法不能抛出比父类方法更多的异常(但子类方法可以不抛出异常)。
 4、存在于父类和子类之间。
 ​5、方法被定义为final不能被重写。
 * @author 徐士宽
 * @date 2017/3/8 14:10
 */
public class Override extends Parent{
    /**
     * 1、方法名、参数、返回值相同。
     */
    public void parentOneTest(){

    }
    public String parentOneTest(String src){
        return src;
    }

    /*****************  1  end    **********************/

    /**
     * 2、子类方法不能缩小父类方法的访问权限。
     * 编译异常
     */
//    private String parentTwoTest(String src){
//        return src;
//    }
    public String parentTwoTest(String src){
        return src;
    }
    /*****************  2  end    **********************/


    public void parentThreeTest(String time){

    }

//    public void parentThreeTest(String time) throws NullPointerException,IOException,SQLException {
//
//    }

    /*****************  3  end    **********************/



    /*****************  4  end    **********************/


    /**
     * ​5、方法被定义为final不能被重写。
     */
//    public void parentFourTest(String time){
//
//    }
    /*****************  5  end    **********************/

}
