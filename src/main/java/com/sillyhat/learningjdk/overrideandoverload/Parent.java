package com.sillyhat.learningjdk.overrideandoverload;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Parent
 *
 * @author 徐士宽
 * @date 2017/3/9 10:28
 */
public class Parent {

    Parent(){

    }

    public void parentOneTest(){

    }

    public String parentOneTest(String src){
        return src;
    }

    protected String parentTwoTest(String src){
        return src;
    }

    public void parentThreeTest(String time) throws IOException,SQLException{

    }

    public final void parentFourTest(String time){

    }
}
